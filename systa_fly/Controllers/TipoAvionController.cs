﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SqlServer.Data;
using SqlServer.Models;

namespace systa_fly.Controllers
{
    public class TipoAvionController : Controller
    {
        private readonly AeropuertoDbContext _context;

        public TipoAvionController(AeropuertoDbContext context)
        {
            _context = context;
        }

        // GET: TipoAvion
        public async Task<IActionResult> Index()
        {
              return _context.TipoAvions != null ? 
                          View(await _context.TipoAvions.ToListAsync()) :
                          Problem("Entity set 'AeropuertoDbContext.TipoAvions'  is null.");
        }

        // GET: TipoAvion/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TipoAvions == null)
            {
                return NotFound();
            }

            var tipoAvion = await _context.TipoAvions
                .FirstOrDefaultAsync(m => m.IdTipoAvion == id);
            if (tipoAvion == null)
            {
                return NotFound();
            }

            return View(tipoAvion);
        }

        // GET: TipoAvion/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoAvion/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdTipoAvion,Tipo,Descripcion")] TipoAvion tipoAvion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipoAvion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipoAvion);
        }

        // GET: TipoAvion/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TipoAvions == null)
            {
                return NotFound();
            }

            var tipoAvion = await _context.TipoAvions.FindAsync(id);
            if (tipoAvion == null)
            {
                return NotFound();
            }
            return View(tipoAvion);
        }

        // POST: TipoAvion/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdTipoAvion,Tipo,Descripcion")] TipoAvion tipoAvion)
        {
            if (id != tipoAvion.IdTipoAvion)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoAvion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoAvionExists(tipoAvion.IdTipoAvion))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipoAvion);
        }

        // GET: TipoAvion/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TipoAvions == null)
            {
                return NotFound();
            }

            var tipoAvion = await _context.TipoAvions
                .FirstOrDefaultAsync(m => m.IdTipoAvion == id);
            if (tipoAvion == null)
            {
                return NotFound();
            }

            return View(tipoAvion);
        }

        // POST: TipoAvion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TipoAvions == null)
            {
                return Problem("Entity set 'AeropuertoDbContext.TipoAvions'  is null.");
            }
            var tipoAvion = await _context.TipoAvions.FindAsync(id);
            if (tipoAvion != null)
            {
                _context.TipoAvions.Remove(tipoAvion);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoAvionExists(int id)
        {
          return (_context.TipoAvions?.Any(e => e.IdTipoAvion == id)).GetValueOrDefault();
        }
    }
}
