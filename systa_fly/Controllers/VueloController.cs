﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SqlServer.Data;
using SqlServer.Models;

namespace systa_fly.Controllers
{
    public class VueloController : Controller
    {
        private readonly AeropuertoDbContext _context;

        public VueloController(AeropuertoDbContext context)
        {
            _context = context;
        }

        // GET: Vuelo
        public async Task<IActionResult> Index()
        {
            var aeropuertoDbContext = _context.Vuelos.Include(v => v.AvionNavigation).Include(v => v.CopilotoAginadoNavigation).Include(v => v.PilotoNavigation).Include(v => v.TipoNavigation);
            return View(await aeropuertoDbContext.ToListAsync());
        }

        // GET: Vuelo/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Vuelos == null)
            {
                return NotFound();
            }

            var vuelo = await _context.Vuelos
                .Include(v => v.AvionNavigation)
                .Include(v => v.CopilotoAginadoNavigation)
                .Include(v => v.PilotoNavigation)
                .Include(v => v.TipoNavigation)
                .FirstOrDefaultAsync(m => m.IdVuelo == id);
            if (vuelo == null)
            {
                return NotFound();
            }

            return View(vuelo);
        }

        // GET: Vuelo/Create
        public IActionResult Create()
        {
            ViewData["Avion"] = _context.Avions.ToList();
			ViewData["CopilotoAginado"] = _context.Pilotos.Where(x => x.TipoNavigation.Tipo.StartsWith("Copiloto")).ToList();
			ViewData["Piloto"] = _context.Pilotos.Where(x => x.TipoNavigation.Tipo.StartsWith("Piloto")).ToList();
			ViewData["Tipo"] = _context.TipoVuelos.ToList();
			return View();
        }

        // POST: Vuelo/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdVuelo,Numero,Fecha,Tipo,Piloto,CopilotoAginado,Avion")] Vuelo vuelo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(vuelo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
			ViewData["Avion"] = _context.Avions.ToList();
			ViewData["CopilotoAginado"] = _context.Pilotos.Where(x => x.TipoNavigation.Tipo.StartsWith("Copiloto")).ToList();
			ViewData["Piloto"] = _context.Pilotos.Where(x => x.TipoNavigation.Tipo.StartsWith("Piloto")).ToList();
			ViewData["Tipo"] = _context.TipoVuelos.ToList();
			return View(vuelo);
        }

        // GET: Vuelo/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Vuelos == null)
            {
                return NotFound();
            }

            var vuelo = await _context.Vuelos.FindAsync(id);
            if (vuelo == null)
            {
                return NotFound();
            }
			ViewData["Avion"] = _context.Avions.ToList();
			ViewData["CopilotoAginado"] = _context.Pilotos.Where(x => x.TipoNavigation.Tipo.StartsWith("Copiloto")).ToList();
			ViewData["Piloto"] = _context.Pilotos.Where(x => x.TipoNavigation.Tipo.StartsWith("Piloto")).ToList();
			ViewData["Tipo"] = _context.TipoVuelos.ToList();
			return View(vuelo);
        }

        // POST: Vuelo/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdVuelo,Numero,Fecha,Tipo,Piloto,CopilotoAginado,Avion")] Vuelo vuelo)
        {
            if (id != vuelo.IdVuelo)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(vuelo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VueloExists(vuelo.IdVuelo))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
			ViewData["Avion"] = _context.Avions.ToList();
			ViewData["CopilotoAginado"] = _context.Pilotos.Where(x => x.TipoNavigation.Tipo.StartsWith("Copiloto")).ToList();
			ViewData["Piloto"] = _context.Pilotos.Where(x => x.TipoNavigation.Tipo.StartsWith("Piloto")).ToList();
			ViewData["Tipo"] = _context.TipoVuelos.ToList();
			return View(vuelo);
        }

        // GET: Vuelo/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Vuelos == null)
            {
                return NotFound();
            }

            var vuelo = await _context.Vuelos
                .Include(v => v.AvionNavigation)
                .Include(v => v.CopilotoAginadoNavigation)
                .Include(v => v.PilotoNavigation)
                .Include(v => v.TipoNavigation)
                .FirstOrDefaultAsync(m => m.IdVuelo == id);
            if (vuelo == null)
            {
                return NotFound();
            }

            return View(vuelo);
        }

        // POST: Vuelo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Vuelos == null)
            {
                return Problem("Entity set 'AeropuertoDbContext.Vuelos'  is null.");
            }
            var vuelo = await _context.Vuelos.FindAsync(id);
            if (vuelo != null)
            {
                _context.Vuelos.Remove(vuelo);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VueloExists(int id)
        {
          return (_context.Vuelos?.Any(e => e.IdVuelo == id)).GetValueOrDefault();
        }
    }
}
