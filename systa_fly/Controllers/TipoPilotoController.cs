﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SqlServer.Data;
using SqlServer.Models;

namespace systa_fly.Controllers
{
    public class TipoPilotoController : Controller
    {
        private readonly AeropuertoDbContext _context;

        public TipoPilotoController(AeropuertoDbContext context)
        {
            _context = context;
        }

        // GET: TipoPiloto
        public async Task<IActionResult> Index()
        {
              return _context.TipoPilotos != null ? 
                          View(await _context.TipoPilotos.ToListAsync()) :
                          Problem("Entity set 'AeropuertoDbContext.TipoPilotos'  is null.");
        }

        // GET: TipoPiloto/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TipoPilotos == null)
            {
                return NotFound();
            }

            var tipoPiloto = await _context.TipoPilotos
                .FirstOrDefaultAsync(m => m.IdTipoPiloto == id);
            if (tipoPiloto == null)
            {
                return NotFound();
            }

            return View(tipoPiloto);
        }

        // GET: TipoPiloto/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoPiloto/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdTipoPiloto,Tipo,Descripcion")] TipoPiloto tipoPiloto)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipoPiloto);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipoPiloto);
        }

        // GET: TipoPiloto/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TipoPilotos == null)
            {
                return NotFound();
            }

            var tipoPiloto = await _context.TipoPilotos.FindAsync(id);
            if (tipoPiloto == null)
            {
                return NotFound();
            }
            return View(tipoPiloto);
        }

        // POST: TipoPiloto/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdTipoPiloto,Tipo,Descripcion")] TipoPiloto tipoPiloto)
        {
            if (id != tipoPiloto.IdTipoPiloto)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoPiloto);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoPilotoExists(tipoPiloto.IdTipoPiloto))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipoPiloto);
        }

        // GET: TipoPiloto/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TipoPilotos == null)
            {
                return NotFound();
            }

            var tipoPiloto = await _context.TipoPilotos
                .FirstOrDefaultAsync(m => m.IdTipoPiloto == id);
            if (tipoPiloto == null)
            {
                return NotFound();
            }

            return View(tipoPiloto);
        }

        // POST: TipoPiloto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TipoPilotos == null)
            {
                return Problem("Entity set 'AeropuertoDbContext.TipoPilotos'  is null.");
            }
            var tipoPiloto = await _context.TipoPilotos.FindAsync(id);
            if (tipoPiloto != null)
            {
                _context.TipoPilotos.Remove(tipoPiloto);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoPilotoExists(int id)
        {
          return (_context.TipoPilotos?.Any(e => e.IdTipoPiloto == id)).GetValueOrDefault();
        }
    }
}
