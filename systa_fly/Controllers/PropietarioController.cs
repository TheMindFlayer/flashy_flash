﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SqlServer.Data;
using SqlServer.Models;

namespace systa_fly.Controllers
{
    public class PropietarioController : Controller
    {
        private readonly AeropuertoDbContext _context;

        public PropietarioController(AeropuertoDbContext context)
        {
            _context = context;
        }

        // GET: Propietario
        public async Task<IActionResult> Index()
        {
            var aeropuertoDbContext = _context.Propietarios.Include(p => p.DatoPropietarioNavigation);
            return View(await aeropuertoDbContext.ToListAsync());
        }

        // GET: Propietario/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Propietarios == null)
            {
                return NotFound();
            }

            var propietario = await _context.Propietarios
                .Include(p => p.DatoPropietarioNavigation)
                .FirstOrDefaultAsync(m => m.IdPropietario == id);
            if (propietario == null)
            {
                return NotFound();
            }

            return View(propietario);
        }

        // GET: Propietario/Create
        public IActionResult Create()
        {
			ViewData["DatoPropietario"] = _context.DatoPersonas.ToList();
            return View();
        }

        // POST: Propietario/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdPropietario,Rif,DatoPropietario")] Propietario propietario)
        {
            if (ModelState.IsValid)
            {
                _context.Add(propietario);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
			ViewData["DatoPropietario"] = _context.DatoPersonas.ToList();
			return View(propietario);
        }

        // GET: Propietario/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Propietarios == null)
            {
                return NotFound();
            }

            var propietario = await _context.Propietarios.FindAsync(id);
            if (propietario == null)
            {
                return NotFound();
            }
            ViewData["DatoPropietario"] = _context.DatoPersonas.ToList();
            return View(propietario);
        }

        // POST: Propietario/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdPropietario,Rif,DatoPropietario")] Propietario propietario)
        {
            if (id != propietario.IdPropietario)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(propietario);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PropietarioExists(propietario.IdPropietario))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DatoPropietario"] = new SelectList(_context.DatoPersonas, "IdPersona", "IdPersona", propietario.DatoPropietario);
            return View(propietario);
        }

        // GET: Propietario/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Propietarios == null)
            {
                return NotFound();
            }

            var propietario = await _context.Propietarios
                .Include(p => p.DatoPropietarioNavigation)
                .FirstOrDefaultAsync(m => m.IdPropietario == id);
            if (propietario == null)
            {
                return NotFound();
            }

            return View(propietario);
        }

        // POST: Propietario/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Propietarios == null)
            {
                return Problem("Entity set 'AeropuertoDbContext.Propietarios'  is null.");
            }
            var propietario = await _context.Propietarios.FindAsync(id);
            if (propietario != null)
            {
                _context.Propietarios.Remove(propietario);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PropietarioExists(int id)
        {
          return (_context.Propietarios?.Any(e => e.IdPropietario == id)).GetValueOrDefault();
        }
    }
}
