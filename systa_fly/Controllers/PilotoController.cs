﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SqlServer.Data;
using SqlServer.Models;

namespace systa_fly.Controllers
{
	public class PilotoController : Controller
	{
		private readonly AeropuertoDbContext _context;

		public PilotoController(AeropuertoDbContext context)
		{
			_context = context;
		}

		// GET: Piloto
		public async Task<IActionResult> Index()
		{
			var aeropuertoDbContext = _context.Pilotos.Include(p => p.DatoPilotoNavigation).Include(p => p.TipoNavigation);
			return View(await aeropuertoDbContext.ToListAsync());
		}

		// GET: Piloto/Details/5
		public async Task<IActionResult> Details(int? id)
		{
			if (id == null || _context.Pilotos == null)
			{
				return NotFound();
			}

			var piloto = await _context.Pilotos
				.Include(p => p.DatoPilotoNavigation)
				.Include(p => p.TipoNavigation)
				.FirstOrDefaultAsync(m => m.IdPiloto == id);
			if (piloto == null)
			{
				return NotFound();
			}

			return View(piloto);
		}

		// GET: Piloto/Create
		public IActionResult Create()
		{
			ViewData["DatoPiloto"] = _context.DatoPersonas.ToList();
			ViewData["Tipo"] = _context.TipoPilotos.ToList();
			return View();
		}

		// POST: Piloto/Create
		// To protect from overposting attacks, enable the specific properties you want to bind to.
		// For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("IdPiloto,Licencia,HorasVuelo,FechaRev,DatoPiloto,Tipo")] Piloto piloto)
		{
			if (ModelState.IsValid)
			{
				_context.Add(piloto);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			ViewData["DatoPiloto"] = _context.DatoPersonas.ToList();
			ViewData["Tipo"] = _context.TipoPilotos.ToList(); 
			return View(piloto);
		}

		// GET: Piloto/Edit/5
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null || _context.Pilotos == null)
			{
				return NotFound();
			}

			var piloto = await _context.Pilotos.FindAsync(id);
			if (piloto == null)
			{
				return NotFound();
			}
			ViewData["DatoPiloto"] = _context.DatoPersonas.ToList();
			ViewData["Tipo"] = _context.TipoPilotos.ToList(); 
			return View(piloto);
		}

		// POST: Piloto/Edit/5
		// To protect from overposting attacks, enable the specific properties you want to bind to.
		// For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, [Bind("IdPiloto,Licencia,HorasVuelo,FechaRev,DatoPiloto,Tipo")] Piloto piloto)
		{
			if (id != piloto.IdPiloto)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(piloto);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!PilotoExists(piloto.IdPiloto))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			ViewData["DatoPiloto"] = _context.DatoPersonas.ToList();
			ViewData["Tipo"] = _context.TipoPilotos.ToList();
			return View(piloto);
		}

		// GET: Piloto/Delete/5
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null || _context.Pilotos == null)
			{
				return NotFound();
			}

			var piloto = await _context.Pilotos
				.Include(p => p.DatoPilotoNavigation)
				.Include(p => p.TipoNavigation)
				.FirstOrDefaultAsync(m => m.IdPiloto == id);
			if (piloto == null)
			{
				return NotFound();
			}

			return View(piloto);
		}

		// POST: Piloto/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			if (_context.Pilotos == null)
			{
				return Problem("Entity set 'AeropuertoDbContext.Pilotos'  is null.");
			}
			var piloto = await _context.Pilotos.FindAsync(id);
			if (piloto != null)
			{
				_context.Pilotos.Remove(piloto);
			}

			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}

		private bool PilotoExists(int id)
		{
			return (_context.Pilotos?.Any(e => e.IdPiloto == id)).GetValueOrDefault();
		}
	}
}
