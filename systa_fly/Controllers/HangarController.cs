﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SqlServer.Data;
using SqlServer.Models;

namespace systa_fly.Controllers
{
    public class HangarController : Controller
    {
        private readonly AeropuertoDbContext _context;

        public HangarController(AeropuertoDbContext context)
        {
            _context = context;
        }

        // GET: Hangar
        public async Task<IActionResult> Index()
        {
              return _context.Hangars != null ? 
                          View(await _context.Hangars.ToListAsync()) :
                          Problem("Entity set 'AeropuertoDbContext.Hangars'  is null.");
        }

        // GET: Hangar/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Hangars == null)
            {
                return NotFound();
            }

            var hangar = await _context.Hangars
                .FirstOrDefaultAsync(m => m.IdHangar == id);
            if (hangar == null)
            {
                return NotFound();
            }

            return View(hangar);
        }

        // GET: Hangar/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Hangar/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdHangar,Codigo,Ubicacion,Costo,Capacidad")] Hangar hangar)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hangar);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(hangar);
        }

        // GET: Hangar/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Hangars == null)
            {
                return NotFound();
            }

            var hangar = await _context.Hangars.FindAsync(id);
            if (hangar == null)
            {
                return NotFound();
            }
            return View(hangar);
        }

        // POST: Hangar/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdHangar,Codigo,Ubicacion,Costo,Capacidad")] Hangar hangar)
        {
            if (id != hangar.IdHangar)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hangar);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HangarExists(hangar.IdHangar))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(hangar);
        }

        // GET: Hangar/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Hangars == null)
            {
                return NotFound();
            }

            var hangar = await _context.Hangars
                .FirstOrDefaultAsync(m => m.IdHangar == id);
            if (hangar == null)
            {
                return NotFound();
            }

            return View(hangar);
        }

        // POST: Hangar/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Hangars == null)
            {
                return Problem("Entity set 'AeropuertoDbContext.Hangars'  is null.");
            }
            var hangar = await _context.Hangars.FindAsync(id);
            if (hangar != null)
            {
                _context.Hangars.Remove(hangar);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HangarExists(int id)
        {
          return (_context.Hangars?.Any(e => e.IdHangar == id)).GetValueOrDefault();
        }
    }
}
