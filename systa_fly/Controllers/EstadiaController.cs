﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SqlServer.Data;
using SqlServer.Models;

namespace systa_fly.Controllers
{
    public class EstadiaController : Controller
    {
        private readonly AeropuertoDbContext _context;

        public EstadiaController(AeropuertoDbContext context)
        {
            _context = context;
        }

        // GET: Estadia
        public async Task<IActionResult> Index()
        {
            var aeropuertoDbContext = _context.Estadia.Include(e => e.AvionNavigation).Include(e => e.HangarNavigation);
            return View(await aeropuertoDbContext.ToListAsync());
        }

        // GET: Estadia/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Estadia == null)
            {
                return NotFound();
            }

            var estadium = await _context.Estadia
                .Include(e => e.AvionNavigation)
                .Include(e => e.HangarNavigation)
                .FirstOrDefaultAsync(m => m.IdEstadia == id);
            if (estadium == null)
            {
                return NotFound();
            }

            return View(estadium);
        }

        // GET: Estadia/Create
        public IActionResult Create()
        {
            ViewData["Avion"] = _context.Avions.ToList();
            ViewData["Hangar"] = _context.Hangars.ToList();
            return View();
        }

        // POST: Estadia/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdEstadia,FechaIn,FechaFin,Costo,Avion,Hangar")] Estadium estadium)
        {
            if (ModelState.IsValid)
            {
				DateTime fechaIn = (DateTime)estadium.FechaIn;
				DateTime fechaOut = (DateTime)estadium.FechaFin;
				int costo = (int)_context.Hangars.Find(estadium.Hangar).Costo;
				double totalDays = Math.Abs(fechaIn.Subtract(fechaOut).TotalDays);
				estadium.Costo = costo * totalDays;
                _context.Add(estadium);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));

            }
			ViewData["Avion"] = _context.Avions.ToList();
			ViewData["Hangar"] = _context.Hangars.ToList();
			return View(estadium);
        }

        // GET: Estadia/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Estadia == null)
            {
                return NotFound();
            }

            var estadium = await _context.Estadia.FindAsync(id);
            if (estadium == null)
            {
                return NotFound();
            }
			ViewData["Avion"] = _context.Avions.ToList();
			ViewData["Hangar"] = _context.Hangars.ToList();
			return View(estadium);
        }

        // POST: Estadia/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdEstadia,FechaIn,FechaFin,Costo,Avion,Hangar")] Estadium estadium)
        {
            if (id != estadium.IdEstadia)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(estadium);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EstadiumExists(estadium.IdEstadia))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
			ViewData["Avion"] = _context.Avions.ToList();
			ViewData["Hangar"] = _context.Hangars.ToList();
			return View(estadium);
        }

        // GET: Estadia/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Estadia == null)
            {
                return NotFound();
            }

            var estadium = await _context.Estadia
                .Include(e => e.AvionNavigation)
                .Include(e => e.HangarNavigation)
                .FirstOrDefaultAsync(m => m.IdEstadia == id);
            if (estadium == null)
            {
                return NotFound();
            }

            return View(estadium);
        }

        // POST: Estadia/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Estadia == null)
            {
                return Problem("Entity set 'AeropuertoDbContext.Estadia'  is null.");
            }
            var estadium = await _context.Estadia.FindAsync(id);
            if (estadium != null)
            {
                _context.Estadia.Remove(estadium);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EstadiumExists(int id)
        {
          return (_context.Estadia?.Any(e => e.IdEstadia == id)).GetValueOrDefault();
        }
    }
}
