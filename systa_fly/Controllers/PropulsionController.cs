﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SqlServer.Data;
using SqlServer.Models;

namespace systa_fly.Controllers
{
    public class PropulsionController : Controller
    {
        private readonly AeropuertoDbContext _context;

        public PropulsionController(AeropuertoDbContext context)
        {
            _context = context;
        }

        // GET: Propulsion
        public async Task<IActionResult> Index()
        {
              return _context.Propulsions != null ? 
                          View(await _context.Propulsions.ToListAsync()) :
                          Problem("Entity set 'AeropuertoDbContext.Propulsions'  is null.");
        }

        // GET: Propulsion/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Propulsions == null)
            {
                return NotFound();
            }

            var propulsion = await _context.Propulsions
                .FirstOrDefaultAsync(m => m.IdPropulsion == id);
            if (propulsion == null)
            {
                return NotFound();
            }

            return View(propulsion);
        }

        // GET: Propulsion/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Propulsion/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdPropulsion,Propulsion1,Descripcion")] Propulsion propulsion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(propulsion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(propulsion);
        }

        // GET: Propulsion/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Propulsions == null)
            {
                return NotFound();
            }

            var propulsion = await _context.Propulsions.FindAsync(id);
            if (propulsion == null)
            {
                return NotFound();
            }
            return View(propulsion);
        }

        // POST: Propulsion/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdPropulsion,Propulsion1,Descripcion")] Propulsion propulsion)
        {
            if (id != propulsion.IdPropulsion)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(propulsion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PropulsionExists(propulsion.IdPropulsion))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(propulsion);
        }

        // GET: Propulsion/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Propulsions == null)
            {
                return NotFound();
            }

            var propulsion = await _context.Propulsions
                .FirstOrDefaultAsync(m => m.IdPropulsion == id);
            if (propulsion == null)
            {
                return NotFound();
            }

            return View(propulsion);
        }

        // POST: Propulsion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Propulsions == null)
            {
                return Problem("Entity set 'AeropuertoDbContext.Propulsions'  is null.");
            }
            var propulsion = await _context.Propulsions.FindAsync(id);
            if (propulsion != null)
            {
                _context.Propulsions.Remove(propulsion);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PropulsionExists(int id)
        {
          return (_context.Propulsions?.Any(e => e.IdPropulsion == id)).GetValueOrDefault();
        }
    }
}
