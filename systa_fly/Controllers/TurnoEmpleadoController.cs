﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SqlServer.Data;
using SqlServer.Models;

namespace systa_fly.Controllers
{
    public class TurnoEmpleadoController : Controller
    {
        private readonly AeropuertoDbContext _context;

        public TurnoEmpleadoController(AeropuertoDbContext context)
        {
            _context = context;
        }

        // GET: TurnoEmpleado
        public async Task<IActionResult> Index()
        {
              return _context.TurnoEmpleados != null ? 
                          View(await _context.TurnoEmpleados.ToListAsync()) :
                          Problem("Entity set 'AeropuertoDbContext.TurnoEmpleados'  is null.");
        }

        // GET: TurnoEmpleado/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TurnoEmpleados == null)
            {
                return NotFound();
            }

            var turnoEmpleado = await _context.TurnoEmpleados
                .FirstOrDefaultAsync(m => m.IdTurnoEmpleado == id);
            if (turnoEmpleado == null)
            {
                return NotFound();
            }

            return View(turnoEmpleado);
        }

        // GET: TurnoEmpleado/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TurnoEmpleado/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdTurnoEmpleado,Turno,Descripcion")] TurnoEmpleado turnoEmpleado)
        {
            if (ModelState.IsValid)
            {
                _context.Add(turnoEmpleado);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(turnoEmpleado);
        }

        // GET: TurnoEmpleado/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TurnoEmpleados == null)
            {
                return NotFound();
            }

            var turnoEmpleado = await _context.TurnoEmpleados.FindAsync(id);
            if (turnoEmpleado == null)
            {
                return NotFound();
            }
            return View(turnoEmpleado);
        }

        // POST: TurnoEmpleado/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdTurnoEmpleado,Turno,Descripcion")] TurnoEmpleado turnoEmpleado)
        {
            if (id != turnoEmpleado.IdTurnoEmpleado)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(turnoEmpleado);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TurnoEmpleadoExists(turnoEmpleado.IdTurnoEmpleado))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(turnoEmpleado);
        }

        // GET: TurnoEmpleado/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TurnoEmpleados == null)
            {
                return NotFound();
            }

            var turnoEmpleado = await _context.TurnoEmpleados
                .FirstOrDefaultAsync(m => m.IdTurnoEmpleado == id);
            if (turnoEmpleado == null)
            {
                return NotFound();
            }

            return View(turnoEmpleado);
        }

        // POST: TurnoEmpleado/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TurnoEmpleados == null)
            {
                return Problem("Entity set 'AeropuertoDbContext.TurnoEmpleados'  is null.");
            }
            var turnoEmpleado = await _context.TurnoEmpleados.FindAsync(id);
            if (turnoEmpleado != null)
            {
                _context.TurnoEmpleados.Remove(turnoEmpleado);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TurnoEmpleadoExists(int id)
        {
          return (_context.TurnoEmpleados?.Any(e => e.IdTurnoEmpleado == id)).GetValueOrDefault();
        }
    }
}
