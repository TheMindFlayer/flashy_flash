﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SqlServer.Data;
using SqlServer.Models;

namespace systa_fly.Controllers
{
    public class AvionController : Controller
    {
        private readonly AeropuertoDbContext _context;

        public AvionController(AeropuertoDbContext context)
        {
            _context = context;
        }

        // GET: Avion
        public async Task<IActionResult> Index()
        {
            var aeropuertoDbContext = _context.Avions.Include(a => a.ModeloNavigation).Include(a => a.TipoNavigation);
            return View(await aeropuertoDbContext.ToListAsync());
        }

        // GET: Avion/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Avions == null)
            {
                return NotFound();
            }

            var avion = await _context.Avions
                .Include(a => a.ModeloNavigation)
                .Include(a => a.TipoNavigation)
                .FirstOrDefaultAsync(m => m.IdAvion == id);
            if (avion == null)
            {
                return NotFound();
            }

            return View(avion);
        }

        // GET: Avion/Create
        public IActionResult Create()
        {
            ViewData["Modelo"] = _context.Modelos.ToList();
            ViewData["Tipo"] = _context.TipoAvions.ToList();
            ViewData["Propietario"] = _context.Propietarios.ToList();
            return View();
        }

        // POST: Avion/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdAvion,Siglas,Capacidad,Tipo,Modelo")] Avion avion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(avion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Modelo"] = new SelectList(_context.Modelos, "IdModelo", "IdModelo", avion.Modelo);
            ViewData["Tipo"] = new SelectList(_context.TipoAvions, "IdTipoAvion", "IdTipoAvion", avion.Tipo);
            return View(avion);
        }

        // GET: Avion/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Avions == null)
            {
                return NotFound();
            }

            var avion = await _context.Avions.FindAsync(id);
            if (avion == null)
            {
                return NotFound();
            }
            ViewData["Modelo"] = new SelectList(_context.Modelos, "IdModelo", "IdModelo", avion.Modelo);
            ViewData["Tipo"] = new SelectList(_context.TipoAvions, "IdTipoAvion", "IdTipoAvion", avion.Tipo);
            return View(avion);
        }

        // POST: Avion/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdAvion,Siglas,Capacidad,Tipo,Modelo")] Avion avion)
        {
            if (id != avion.IdAvion)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(avion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AvionExists(avion.IdAvion))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Modelo"] = new SelectList(_context.Modelos, "IdModelo", "IdModelo", avion.Modelo);
            ViewData["Tipo"] = new SelectList(_context.TipoAvions, "IdTipoAvion", "IdTipoAvion", avion.Tipo);
            return View(avion);
        }

        // GET: Avion/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Avions == null)
            {
                return NotFound();
            }

            var avion = await _context.Avions
                .Include(a => a.ModeloNavigation)
                .Include(a => a.TipoNavigation)
                .FirstOrDefaultAsync(m => m.IdAvion == id);
            if (avion == null)
            {
                return NotFound();
            }

            return View(avion);
        }

        // POST: Avion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Avions == null)
            {
                return Problem("Entity set 'AeropuertoDbContext.Avions'  is null.");
            }
            var avion = await _context.Avions.FindAsync(id);
            if (avion != null)
            {
                _context.Avions.Remove(avion);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AvionExists(int id)
        {
          return (_context.Avions?.Any(e => e.IdAvion == id)).GetValueOrDefault();
        }
    }
}
