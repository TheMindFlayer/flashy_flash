﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SqlServer.Data;
using SqlServer.Models;

namespace systa_fly.Controllers
{
    public class TipoVueloController : Controller
    {
        private readonly AeropuertoDbContext _context;

        public TipoVueloController(AeropuertoDbContext context)
        {
            _context = context;
        }

        // GET: TipoVuelo
        public async Task<IActionResult> Index()
        {
              return _context.TipoVuelos != null ? 
                          View(await _context.TipoVuelos.ToListAsync()) :
                          Problem("Entity set 'AeropuertoDbContext.TipoVuelos'  is null.");
        }

        // GET: TipoVuelo/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TipoVuelos == null)
            {
                return NotFound();
            }

            var tipoVuelo = await _context.TipoVuelos
                .FirstOrDefaultAsync(m => m.IdTipoVuelo == id);
            if (tipoVuelo == null)
            {
                return NotFound();
            }

            return View(tipoVuelo);
        }

        // GET: TipoVuelo/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoVuelo/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdTipoVuelo,Tipo,Descripcion")] TipoVuelo tipoVuelo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipoVuelo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipoVuelo);
        }

        // GET: TipoVuelo/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TipoVuelos == null)
            {
                return NotFound();
            }

            var tipoVuelo = await _context.TipoVuelos.FindAsync(id);
            if (tipoVuelo == null)
            {
                return NotFound();
            }
            return View(tipoVuelo);
        }

        // POST: TipoVuelo/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdTipoVuelo,Tipo,Descripcion")] TipoVuelo tipoVuelo)
        {
            if (id != tipoVuelo.IdTipoVuelo)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoVuelo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoVueloExists(tipoVuelo.IdTipoVuelo))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipoVuelo);
        }

        // GET: TipoVuelo/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TipoVuelos == null)
            {
                return NotFound();
            }

            var tipoVuelo = await _context.TipoVuelos
                .FirstOrDefaultAsync(m => m.IdTipoVuelo == id);
            if (tipoVuelo == null)
            {
                return NotFound();
            }

            return View(tipoVuelo);
        }

        // POST: TipoVuelo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TipoVuelos == null)
            {
                return Problem("Entity set 'AeropuertoDbContext.TipoVuelos'  is null.");
            }
            var tipoVuelo = await _context.TipoVuelos.FindAsync(id);
            if (tipoVuelo != null)
            {
                _context.TipoVuelos.Remove(tipoVuelo);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoVueloExists(int id)
        {
          return (_context.TipoVuelos?.Any(e => e.IdTipoVuelo == id)).GetValueOrDefault();
        }
    }
}
