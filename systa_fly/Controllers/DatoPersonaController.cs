﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SqlServer.Data;
using SqlServer.Models;

namespace systa_fly.Controllers
{
    public class DatoPersonaController : Controller
    {
        private readonly AeropuertoDbContext _context;

        public DatoPersonaController(AeropuertoDbContext context)
        {
            _context = context;
        }

        // GET: DatoPersona
        public async Task<IActionResult> Index()
        {
              return _context.DatoPersonas != null ? 
                          View(await _context.DatoPersonas.ToListAsync()) :
                          Problem("Entity set 'AeropuertoDbContext.DatoPersonas'  is null.");
        }

        // GET: DatoPersona/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.DatoPersonas == null)
            {
                return NotFound();
            }

            var datoPersona = await _context.DatoPersonas
                .FirstOrDefaultAsync(m => m.IdPersona == id);
            if (datoPersona == null)
            {
                return NotFound();
            }

            return View(datoPersona);
        }

        // GET: DatoPersona/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DatoPersona/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdPersona,Cedula,NombreCompleto")] DatoPersona datoPersona)
        {
            if (ModelState.IsValid)
            {
                _context.Add(datoPersona);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(datoPersona);
        }

        // GET: DatoPersona/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.DatoPersonas == null)
            {
                return NotFound();
            }

            var datoPersona = await _context.DatoPersonas.FindAsync(id);
            if (datoPersona == null)
            {
                return NotFound();
            }
            return View(datoPersona);
        }

        // POST: DatoPersona/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdPersona,Cedula,NombreCompleto")] DatoPersona datoPersona)
        {
            if (id != datoPersona.IdPersona)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(datoPersona);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DatoPersonaExists(datoPersona.IdPersona))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(datoPersona);
        }

        // GET: DatoPersona/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.DatoPersonas == null)
            {
                return NotFound();
            }

            var datoPersona = await _context.DatoPersonas
                .FirstOrDefaultAsync(m => m.IdPersona == id);
            if (datoPersona == null)
            {
                return NotFound();
            }

            return View(datoPersona);
        }

        // POST: DatoPersona/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.DatoPersonas == null)
            {
                return Problem("Entity set 'AeropuertoDbContext.DatoPersonas'  is null.");
            }
            var datoPersona = await _context.DatoPersonas.FindAsync(id);
            if (datoPersona != null)
            {
                _context.DatoPersonas.Remove(datoPersona);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DatoPersonaExists(int id)
        {
          return (_context.DatoPersonas?.Any(e => e.IdPersona == id)).GetValueOrDefault();
        }
    }
}
