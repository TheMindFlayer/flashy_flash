﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SqlServer.Data;
using SqlServer.Models;

namespace systa_fly.Controllers
{
	public class EmpleadoController : Controller
	{
		private readonly AeropuertoDbContext _context;

		public EmpleadoController(AeropuertoDbContext context)
		{
			_context = context;
		}

		// GET: Empleado
		public async Task<IActionResult> Index()
		{
			var aeropuertoDbContext = _context.Empleados.Include(e => e.DatoEmpleadoNavigation).Include(e => e.HangarNavigation).Include(e => e.TipoNavigation).Include(e => e.TurnoNavigation);
			return View(await aeropuertoDbContext.ToListAsync());
		}

		// GET: Empleado/Details/5
		public async Task<IActionResult> Details(int? id)
		{
			if (id == null || _context.Empleados == null)
			{
				return NotFound();
			}

			var empleado = await _context.Empleados
				.Include(e => e.DatoEmpleadoNavigation)
				.Include(e => e.HangarNavigation)
				.Include(e => e.TipoNavigation)
				.Include(e => e.TurnoNavigation)
				.FirstOrDefaultAsync(m => m.IdEmpleado == id);
			if (empleado == null)
			{
				return NotFound();
			}

			return View(empleado);
		}

		// GET: Empleado/Create
		public IActionResult Create()
		{
			ViewData["DatoEmpleado"] = _context.DatoPersonas.ToList();
			ViewData["Hangar"] = _context.Hangars.ToList();
			ViewData["Tipo"] = _context.TipoEmpleados.ToList();
			ViewData["Turno"] = _context.TurnoEmpleados.ToList();
			return View();
		}

		// POST: Empleado/Create
		// To protect from overposting attacks, enable the specific properties you want to bind to.
		// For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("IdEmpleado,Salario,Turno,Tipo,DatoEmpleado,Hangar")] Empleado empleado)
		{
			if (ModelState.IsValid)
			{
				_context.Add(empleado);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			ViewData["DatoEmpleado"] = _context.DatoPersonas.ToList();
			ViewData["Hangar"] = _context.Hangars.ToList();
			ViewData["Tipo"] = _context.TipoEmpleados.ToList();
			ViewData["Turno"] = _context.TurnoEmpleados.ToList(); 
			return View(empleado);
		}

		// GET: Empleado/Edit/5
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null || _context.Empleados == null)
			{
				return NotFound();
			}

			var empleado = await _context.Empleados.FindAsync(id);
			if (empleado == null)
			{
				return NotFound();
			}
			ViewData["DatoEmpleado"] = _context.DatoPersonas.ToList();
			ViewData["Hangar"] = _context.Hangars.ToList();
			ViewData["Tipo"] = _context.TipoEmpleados.ToList();
			ViewData["Turno"] = _context.TurnoEmpleados.ToList(); 
			return View(empleado);
		}

		// POST: Empleado/Edit/5
		// To protect from overposting attacks, enable the specific properties you want to bind to.
		// For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, [Bind("IdEmpleado,Salario,Turno,Tipo,DatoEmpleado,Hangar")] Empleado empleado)
		{
			if (id != empleado.IdEmpleado)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(empleado);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!EmpleadoExists(empleado.IdEmpleado))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			ViewData["DatoEmpleado"] = _context.DatoPersonas.ToList();
			ViewData["Hangar"] = _context.Hangars.ToList();
			ViewData["Tipo"] = _context.TipoEmpleados.ToList();
			ViewData["Turno"] = _context.TurnoEmpleados.ToList(); 
			return View(empleado);
		}

		// GET: Empleado/Delete/5
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null || _context.Empleados == null)
			{
				return NotFound();
			}

			var empleado = await _context.Empleados
				.Include(e => e.DatoEmpleadoNavigation)
				.Include(e => e.HangarNavigation)
				.Include(e => e.TipoNavigation)
				.Include(e => e.TurnoNavigation)
				.FirstOrDefaultAsync(m => m.IdEmpleado == id);
			if (empleado == null)
			{
				return NotFound();
			}

			return View(empleado);
		}

		// POST: Empleado/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			if (_context.Empleados == null)
			{
				return Problem("Entity set 'AeropuertoDbContext.Empleados'  is null.");
			}
			var empleado = await _context.Empleados.FindAsync(id);
			if (empleado != null)
			{
				_context.Empleados.Remove(empleado);
			}

			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}

		private bool EmpleadoExists(int id)
		{
			return (_context.Empleados?.Any(e => e.IdEmpleado == id)).GetValueOrDefault();
		}
	}
}
