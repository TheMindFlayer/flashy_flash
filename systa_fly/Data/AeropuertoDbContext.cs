﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using SqlServer.Models;

namespace SqlServer.Data
{
    public partial class AeropuertoDbContext : DbContext
    {
        public AeropuertoDbContext()
        {
        }

        public AeropuertoDbContext(DbContextOptions<AeropuertoDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Avion> Avions { get; set; } = null!;
        public virtual DbSet<AvionPropietario> AvionPropietarios { get; set; } = null!;
        public virtual DbSet<DatoPersona> DatoPersonas { get; set; } = null!;
        public virtual DbSet<Empleado> Empleados { get; set; } = null!;
        public virtual DbSet<Estadium> Estadia { get; set; } = null!;
        public virtual DbSet<Hangar> Hangars { get; set; } = null!;
        public virtual DbSet<Modelo> Modelos { get; set; } = null!;
        public virtual DbSet<Piloto> Pilotos { get; set; } = null!;
        public virtual DbSet<Propietario> Propietarios { get; set; } = null!;
        public virtual DbSet<Propulsion> Propulsions { get; set; } = null!;
        public virtual DbSet<TipoAvion> TipoAvions { get; set; } = null!;
        public virtual DbSet<TipoEmpleado> TipoEmpleados { get; set; } = null!;
        public virtual DbSet<TipoPiloto> TipoPilotos { get; set; } = null!;
        public virtual DbSet<TipoVuelo> TipoVuelos { get; set; } = null!;
        public virtual DbSet<TurnoEmpleado> TurnoEmpleados { get; set; } = null!;
        public virtual DbSet<Vuelo> Vuelos { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Avion>(entity =>
            {
                entity.HasKey(e => e.IdAvion)
                    .HasName("PK__avion__66D8A4F3EE107773");

                entity.ToTable("avion");

                entity.Property(e => e.IdAvion).HasColumnName("id_avion");

                entity.Property(e => e.Capacidad).HasColumnName("capacidad");

                entity.Property(e => e.Modelo).HasColumnName("modelo");

                entity.Property(e => e.Siglas)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("siglas");

                entity.Property(e => e.Tipo).HasColumnName("tipo");

                entity.HasOne(d => d.ModeloNavigation)
                    .WithMany(p => p.Avions)
                    .HasForeignKey(d => d.Modelo)
                    .HasConstraintName("FK__avion__modelo__2B3F6F97");

                entity.HasOne(d => d.TipoNavigation)
                    .WithMany(p => p.Avions)
                    .HasForeignKey(d => d.Tipo)
                    .HasConstraintName("FK__avion__tipo__2A4B4B5E");
            });

            modelBuilder.Entity<AvionPropietario>(entity =>
            {
                entity.HasKey(e => e.IdAvionPropietario)
                    .HasName("PK__avion_pr__D02F541091FEED4D");

                entity.ToTable("avion_propietario");

                entity.Property(e => e.IdAvionPropietario).HasColumnName("id_avionPropietario");

                entity.Property(e => e.AvionId).HasColumnName("avion_id");

                entity.Property(e => e.PropietarioId).HasColumnName("propietario_id");

                entity.HasOne(d => d.Avion)
                    .WithMany(p => p.AvionPropietarios)
                    .HasForeignKey(d => d.AvionId)
                    .HasConstraintName("FK__avion_pro__avion__48CFD27E");

                entity.HasOne(d => d.Propietario)
                    .WithMany(p => p.AvionPropietarios)
                    .HasForeignKey(d => d.PropietarioId)
                    .HasConstraintName("FK__avion_pro__propi__47DBAE45");
            });

            modelBuilder.Entity<DatoPersona>(entity =>
            {
                entity.HasKey(e => e.IdPersona)
                    .HasName("PK__datoPers__228148B01BCCF0EA");

                entity.ToTable("datoPersona");

                entity.Property(e => e.IdPersona).HasColumnName("id_persona");

                entity.Property(e => e.Cedula)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("cedula");

                entity.Property(e => e.NombreCompleto)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("nombre_completo");
            });

            modelBuilder.Entity<Empleado>(entity =>
            {
                entity.HasKey(e => e.IdEmpleado)
                    .HasName("PK__empleado__88B51394804CCBCB");

                entity.ToTable("empleado");

                entity.Property(e => e.IdEmpleado).HasColumnName("id_empleado");

                entity.Property(e => e.DatoEmpleado).HasColumnName("dato_empleado");

                entity.Property(e => e.Hangar).HasColumnName("hangar");

                entity.Property(e => e.Salario).HasColumnName("salario");

                entity.Property(e => e.Tipo).HasColumnName("tipo");

                entity.Property(e => e.Turno).HasColumnName("turno");

                entity.HasOne(d => d.DatoEmpleadoNavigation)
                    .WithMany(p => p.Empleados)
                    .HasForeignKey(d => d.DatoEmpleado)
                    .HasConstraintName("FK__empleado__dato_e__4F7CD00D");

                entity.HasOne(d => d.HangarNavigation)
                    .WithMany(p => p.Empleados)
                    .HasForeignKey(d => d.Hangar)
                    .HasConstraintName("FK__empleado__hangar__5070F446");

                entity.HasOne(d => d.TipoNavigation)
                    .WithMany(p => p.Empleados)
                    .HasForeignKey(d => d.Tipo)
                    .HasConstraintName("FK__empleado__tipo__4E88ABD4");

                entity.HasOne(d => d.TurnoNavigation)
                    .WithMany(p => p.Empleados)
                    .HasForeignKey(d => d.Turno)
                    .HasConstraintName("FK__empleado__turno__4D94879B");
            });

            modelBuilder.Entity<Estadium>(entity =>
            {
                entity.HasKey(e => e.IdEstadia)
                    .HasName("PK__estadia__3F2F78D7AD8DB8CD");

                entity.ToTable("estadia");

                entity.Property(e => e.IdEstadia).HasColumnName("id_estadia");

                entity.Property(e => e.Avion).HasColumnName("avion");

                entity.Property(e => e.Costo).HasColumnName("costo");

                entity.Property(e => e.FechaFin)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_fin");

                entity.Property(e => e.FechaIn)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_in");

                entity.Property(e => e.Hangar).HasColumnName("hangar");

                entity.HasOne(d => d.AvionNavigation)
                    .WithMany(p => p.Estadia)
                    .HasForeignKey(d => d.Avion)
                    .HasConstraintName("FK__estadia__avion__300424B4");

                entity.HasOne(d => d.HangarNavigation)
                    .WithMany(p => p.Estadia)
                    .HasForeignKey(d => d.Hangar)
                    .HasConstraintName("FK__estadia__hangar__30F848ED");
            });

            modelBuilder.Entity<Hangar>(entity =>
            {
                entity.HasKey(e => e.IdHangar)
                    .HasName("PK__hangar__243A14E254191B6D");

                entity.ToTable("hangar");

                entity.Property(e => e.IdHangar).HasColumnName("id_hangar");

                entity.Property(e => e.Capacidad).HasColumnName("capacidad");

                entity.Property(e => e.Codigo)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("codigo");

                entity.Property(e => e.Costo).HasColumnName("costo");

                entity.Property(e => e.Ubicacion)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ubicacion");
            });

            modelBuilder.Entity<Modelo>(entity =>
            {
                entity.HasKey(e => e.IdModelo)
                    .HasName("PK__modelo__B3BFCFF16A57E3C5");

                entity.ToTable("modelo");

                entity.Property(e => e.IdModelo).HasColumnName("id_modelo");

                entity.Property(e => e.Codigo)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("codigo");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("descripcion");

                entity.Property(e => e.Motores).HasColumnName("motores");

                entity.Property(e => e.Peso).HasColumnName("peso");

                entity.Property(e => e.Propulsion).HasColumnName("propulsion");

                entity.HasOne(d => d.PropulsionNavigation)
                    .WithMany(p => p.Modelos)
                    .HasForeignKey(d => d.Propulsion)
                    .HasConstraintName("FK__modelo__propulsi__25869641");
            });

            modelBuilder.Entity<Piloto>(entity =>
            {
                entity.HasKey(e => e.IdPiloto)
                    .HasName("PK__piloto__93ED5235D0AB4F2D");

                entity.ToTable("piloto");

                entity.Property(e => e.IdPiloto).HasColumnName("id_piloto");

                entity.Property(e => e.DatoPiloto).HasColumnName("dato_piloto");

                entity.Property(e => e.FechaRev)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha_rev");

                entity.Property(e => e.HorasVuelo).HasColumnName("horas_vuelo");

                entity.Property(e => e.Licencia)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("licencia");

                entity.Property(e => e.Tipo).HasColumnName("tipo");

                entity.HasOne(d => d.DatoPilotoNavigation)
                    .WithMany(p => p.Pilotos)
                    .HasForeignKey(d => d.DatoPiloto)
                    .HasConstraintName("FK__piloto__dato_pil__37A5467C");

                entity.HasOne(d => d.TipoNavigation)
                    .WithMany(p => p.Pilotos)
                    .HasForeignKey(d => d.Tipo)
                    .HasConstraintName("FK__piloto__tipo__38996AB5");
            });

            modelBuilder.Entity<Propietario>(entity =>
            {
                entity.HasKey(e => e.IdPropietario)
                    .HasName("PK__propieta__D2E569377E2257C1");

                entity.ToTable("propietario");

                entity.Property(e => e.IdPropietario).HasColumnName("id_propietario");

                entity.Property(e => e.DatoPropietario).HasColumnName("dato_propietario");

                entity.Property(e => e.Rif)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("rif");

                entity.HasOne(d => d.DatoPropietarioNavigation)
                    .WithMany(p => p.Propietarios)
                    .HasForeignKey(d => d.DatoPropietario)
                    .HasConstraintName("FK__propietar__dato___44FF419A");
            });

            modelBuilder.Entity<Propulsion>(entity =>
            {
                entity.HasKey(e => e.IdPropulsion)
                    .HasName("PK__propulsi__2A4D122318B4B930");

                entity.ToTable("propulsion");

                entity.Property(e => e.IdPropulsion).HasColumnName("id_propulsion");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("descripcion");

                entity.Property(e => e.Propulsion1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("propulsion");
            });

            modelBuilder.Entity<TipoAvion>(entity =>
            {
                entity.HasKey(e => e.IdTipoAvion)
                    .HasName("PK__tipo_avi__446DB5A6B16CD293");

                entity.ToTable("tipo_avion");

                entity.Property(e => e.IdTipoAvion).HasColumnName("id_tipoAvion");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("descripcion");

                entity.Property(e => e.Tipo)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("tipo");
            });

            modelBuilder.Entity<TipoEmpleado>(entity =>
            {
                entity.HasKey(e => e.IdTipoEmpleado)
                    .HasName("PK__tipo_emp__2CADC2044E37755C");

                entity.ToTable("tipo_empleado");

                entity.Property(e => e.IdTipoEmpleado).HasColumnName("id_tipoEmpleado");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("descripcion");

                entity.Property(e => e.Tipo)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("tipo");
            });

            modelBuilder.Entity<TipoPiloto>(entity =>
            {
                entity.HasKey(e => e.IdTipoPiloto)
                    .HasName("PK__tipo_pil__3EAB56C138699269");

                entity.ToTable("tipo_piloto");

                entity.Property(e => e.IdTipoPiloto).HasColumnName("id_tipoPiloto");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("descripcion");

                entity.Property(e => e.Tipo)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("tipo");
            });

            modelBuilder.Entity<TipoVuelo>(entity =>
            {
                entity.HasKey(e => e.IdTipoVuelo)
                    .HasName("PK__tipo_vue__7DBF6BB1985F7D7A");

                entity.ToTable("tipo_vuelo");

                entity.Property(e => e.IdTipoVuelo).HasColumnName("id_tipoVuelo");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("descripcion");

                entity.Property(e => e.Tipo)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("tipo");
            });

            modelBuilder.Entity<TurnoEmpleado>(entity =>
            {
                entity.HasKey(e => e.IdTurnoEmpleado)
                    .HasName("PK__turno_em__AF913740E550BD47");

                entity.ToTable("turno_empleado");

                entity.Property(e => e.IdTurnoEmpleado).HasColumnName("id_turnoEmpleado");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("descripcion");

                entity.Property(e => e.Turno)
                    .HasMaxLength(75)
                    .IsUnicode(false)
                    .HasColumnName("turno");
            });

            modelBuilder.Entity<Vuelo>(entity =>
            {
                entity.HasKey(e => e.IdVuelo)
                    .HasName("PK__vuelo__CA179BA26CAB7235");

                entity.ToTable("vuelo");

                entity.Property(e => e.IdVuelo).HasColumnName("id_vuelo");

                entity.Property(e => e.Avion).HasColumnName("avion");

                entity.Property(e => e.CopilotoAginado).HasColumnName("copiloto_aginado");

                entity.Property(e => e.Fecha)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha");

                entity.Property(e => e.Numero)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("numero");

                entity.Property(e => e.Piloto).HasColumnName("piloto");

                entity.Property(e => e.Tipo).HasColumnName("tipo");

                entity.HasOne(d => d.AvionNavigation)
                    .WithMany(p => p.Vuelos)
                    .HasForeignKey(d => d.Avion)
                    .HasConstraintName("FK__vuelo__avion__403A8C7D");

                entity.HasOne(d => d.CopilotoAginadoNavigation)
                    .WithMany(p => p.VueloCopilotoAginadoNavigations)
                    .HasForeignKey(d => d.CopilotoAginado)
                    .HasConstraintName("FK__vuelo__copiloto___3F466844");

                entity.HasOne(d => d.PilotoNavigation)
                    .WithMany(p => p.VueloPilotoNavigations)
                    .HasForeignKey(d => d.Piloto)
                    .HasConstraintName("FK__vuelo__piloto__3E52440B");

                entity.HasOne(d => d.TipoNavigation)
                    .WithMany(p => p.Vuelos)
                    .HasForeignKey(d => d.Tipo)
                    .HasConstraintName("FK__vuelo__tipo__3D5E1FD2");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
