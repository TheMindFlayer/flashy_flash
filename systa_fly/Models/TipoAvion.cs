﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class TipoAvion
    {
        public TipoAvion()
        {
            Avions = new HashSet<Avion>();
        }

        public int IdTipoAvion { get; set; }
        public string? Tipo { get; set; }
        public string? Descripcion { get; set; }

        public virtual ICollection<Avion> Avions { get; set; }
    }
}
