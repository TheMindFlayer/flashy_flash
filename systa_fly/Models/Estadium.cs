﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class Estadium
    {
        public int IdEstadia { get; set; }
        public DateTime? FechaIn { get; set; }
        public DateTime? FechaFin { get; set; }
        public double? Costo { get; set; }
        public int? Avion { get; set; }
        public int? Hangar { get; set; }

        public virtual Avion? AvionNavigation { get; set; }
        public virtual Hangar? HangarNavigation { get; set; }
    }
}
