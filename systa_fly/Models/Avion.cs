﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class Avion
    {
        public Avion()
        {
            AvionPropietarios = new HashSet<AvionPropietario>();
            Estadia = new HashSet<Estadium>();
            Vuelos = new HashSet<Vuelo>();
        }

        public int IdAvion { get; set; }
        public string? Siglas { get; set; }
        public int? Capacidad { get; set; }
        public int? Tipo { get; set; }
        public int? Modelo { get; set; }

        public virtual Modelo? ModeloNavigation { get; set; }
        public virtual TipoAvion? TipoNavigation { get; set; }
        public virtual ICollection<AvionPropietario> AvionPropietarios { get; set; }
        public virtual ICollection<Estadium> Estadia { get; set; }
        public virtual ICollection<Vuelo> Vuelos { get; set; }
    }
}
