﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class TipoVuelo
    {
        public TipoVuelo()
        {
            Vuelos = new HashSet<Vuelo>();
        }

        public int IdTipoVuelo { get; set; }
        public string? Tipo { get; set; }
        public string? Descripcion { get; set; }

        public virtual ICollection<Vuelo> Vuelos { get; set; }
    }
}
