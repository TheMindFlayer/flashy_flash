﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class Modelo
    {
        public Modelo()
        {
            Avions = new HashSet<Avion>();
        }

        public int IdModelo { get; set; }
        public string? Codigo { get; set; }
        public string? Descripcion { get; set; }
        public int? Propulsion { get; set; }
        public int? Motores { get; set; }
        public double? Peso { get; set; }

        public virtual Propulsion? PropulsionNavigation { get; set; }
        public virtual ICollection<Avion> Avions { get; set; }
    }
}
