﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class DatoPersona
    {
        public DatoPersona()
        {
            Empleados = new HashSet<Empleado>();
            Pilotos = new HashSet<Piloto>();
            Propietarios = new HashSet<Propietario>();
        }

        public int IdPersona { get; set; }
        public string? Cedula { get; set; }
        public string? NombreCompleto { get; set; }

        public virtual ICollection<Empleado> Empleados { get; set; }
        public virtual ICollection<Piloto> Pilotos { get; set; }
        public virtual ICollection<Propietario> Propietarios { get; set; }
    }
}
