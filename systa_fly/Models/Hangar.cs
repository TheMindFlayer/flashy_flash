﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class Hangar
    {
        public Hangar()
        {
            Empleados = new HashSet<Empleado>();
            Estadia = new HashSet<Estadium>();
        }

        public int IdHangar { get; set; }
        public string? Codigo { get; set; }
        public string? Ubicacion { get; set; }
        public int? Costo { get; set; }
        public int? Capacidad { get; set; }

        public virtual ICollection<Empleado> Empleados { get; set; }
        public virtual ICollection<Estadium> Estadia { get; set; }
    }
}
