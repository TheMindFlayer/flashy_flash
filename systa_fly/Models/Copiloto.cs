﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class Copiloto
    {
        public Copiloto()
        {
            Vuelos = new HashSet<Vuelo>();
        }

        public int IdCopiloto { get; set; }
        public int? Piloto { get; set; }

        public virtual Piloto? PilotoNavigation { get; set; }
        public virtual ICollection<Vuelo> Vuelos { get; set; }
    }
}
