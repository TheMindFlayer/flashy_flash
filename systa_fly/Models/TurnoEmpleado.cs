﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class TurnoEmpleado
    {
        public TurnoEmpleado()
        {
            Empleados = new HashSet<Empleado>();
        }

        public int IdTurnoEmpleado { get; set; }
        public string? Turno { get; set; }
        public string? Descripcion { get; set; }

        public virtual ICollection<Empleado> Empleados { get; set; }
    }
}
