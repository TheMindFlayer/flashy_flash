﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class Piloto
    {
        public Piloto()
        {
            VueloCopilotoAginadoNavigations = new HashSet<Vuelo>();
            VueloPilotoNavigations = new HashSet<Vuelo>();
        }

        public int IdPiloto { get; set; }
        public string? Licencia { get; set; }
        public int? HorasVuelo { get; set; }
        public DateTime? FechaRev { get; set; }
        public int? DatoPiloto { get; set; }
        public int? Tipo { get; set; }

        public virtual DatoPersona? DatoPilotoNavigation { get; set; }
        public virtual TipoPiloto? TipoNavigation { get; set; }
        public virtual ICollection<Vuelo> VueloCopilotoAginadoNavigations { get; set; }
        public virtual ICollection<Vuelo> VueloPilotoNavigations { get; set; }
    }
}
