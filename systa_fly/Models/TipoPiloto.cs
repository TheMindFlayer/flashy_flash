﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class TipoPiloto
    {
        public TipoPiloto()
        {
            Pilotos = new HashSet<Piloto>();
        }

        public int IdTipoPiloto { get; set; }
        public string? Tipo { get; set; }
        public string? Descripcion { get; set; }

        public virtual ICollection<Piloto> Pilotos { get; set; }
    }
}
