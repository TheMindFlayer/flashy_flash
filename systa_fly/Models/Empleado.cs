﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class Empleado
    {
        public int IdEmpleado { get; set; }
        public int? Salario { get; set; }
        public int? Turno { get; set; }
        public int? Tipo { get; set; }
        public int? DatoEmpleado { get; set; }
        public int? Hangar { get; set; }

        public virtual DatoPersona? DatoEmpleadoNavigation { get; set; }
        public virtual Hangar? HangarNavigation { get; set; }
        public virtual TipoEmpleado? TipoNavigation { get; set; }
        public virtual TurnoEmpleado? TurnoNavigation { get; set; }
    }
}
