﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class Propulsion
    {
        public Propulsion()
        {
            Modelos = new HashSet<Modelo>();
        }

        public int IdPropulsion { get; set; }
        public string? Propulsion1 { get; set; }
        public string? Descripcion { get; set; }

        public virtual ICollection<Modelo> Modelos { get; set; }
    }
}
