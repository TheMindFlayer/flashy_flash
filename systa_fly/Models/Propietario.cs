﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class Propietario
    {
        public Propietario()
        {
            AvionPropietarios = new HashSet<AvionPropietario>();
        }

        public int IdPropietario { get; set; }
        public string? Rif { get; set; }
        public int? DatoPropietario { get; set; }

        public virtual DatoPersona? DatoPropietarioNavigation { get; set; }
        public virtual ICollection<AvionPropietario> AvionPropietarios { get; set; }
    }
}
