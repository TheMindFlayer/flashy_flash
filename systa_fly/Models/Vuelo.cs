﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class Vuelo
    {
        public int IdVuelo { get; set; }
        public string? Numero { get; set; }
        public DateTime? Fecha { get; set; }
        public int? Tipo { get; set; }
        public int? Piloto { get; set; }
        public int? CopilotoAginado { get; set; }
        public int? Avion { get; set; }

        public virtual Avion? AvionNavigation { get; set; }
        public virtual Piloto? CopilotoAginadoNavigation { get; set; }
        public virtual Piloto? PilotoNavigation { get; set; }
        public virtual TipoVuelo? TipoNavigation { get; set; }
    }
}
