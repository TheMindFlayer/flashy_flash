﻿using System;
using System.Collections.Generic;

namespace SqlServer.Models
{
    public partial class AvionPropietario
    {
        public int IdAvionPropietario { get; set; }
        public int? PropietarioId { get; set; }
        public int? AvionId { get; set; }

        public virtual Avion? Avion { get; set; }
        public virtual Propietario? Propietario { get; set; }
    }
}
