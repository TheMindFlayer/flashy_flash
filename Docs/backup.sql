EXEC sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'

-- Table [dbo].[avion_propietario] --

-- Table [dbo].[propulsion] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[propulsion]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[propulsion] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[propulsion] ([id_propulsion], [descripcion], [propulsion]) VALUES
(1, N'Se basa en utilizar el aire para que entre a gran velocidad en la turbina', N'Motor de reacción'), 
(2, N'Son motores térmicos en los que los gases generados en la reacción exotérmica resultante de un proceso de combustión empujan un émbolo o pistón en un movimiento alternativo', N'Turbo hélice');
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[propulsion]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[propulsion] OFF;
GO

-- Table [dbo].[modelo] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[modelo]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[modelo] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[modelo] ([id_modelo], [codigo], [descripcion], [motores], [peso], [propulsion]) VALUES
(1, N'MS-21-200', N'Ruso. Transporte de pasajeros y carga en vuelos internos e internacionales', 2, 72.56, 1), 
(2, N'MS-21-300', N'Ruso. Transporte de pasajeros y carga en vuelos internos e internacionales', 2, 79.25, 1), 
(3, N'B703', N'Boeing 707 Freighter. Avión comercial de cuatro motores de reacción con fuselaje estrecho, desarrollado por Boeing a principios de los años 50. ', 4, 116.57, 1);
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[modelo]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[modelo] OFF;
GO

-- Table [dbo].[tipo_avion] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[tipo_avion]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[tipo_avion] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[tipo_avion] ([id_tipoAvion], [descripcion], [tipo]) VALUES
(1, N'Aviones para vuelos de pasajeros', N'Comersial'), 
(2, N'Paqueteria y objetos de gran tamaño', N'Carga'), 
(3, N'Funciones de ataque y defensa', N'Militares ');
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[tipo_avion]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[tipo_avion] OFF;
GO

-- Table [dbo].[avion] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[avion]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[avion] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[avion] ([id_avion], [capacidad], [modelo], [siglas], [tipo]) VALUES
(1, 179, 3, N'Boeing 707 Freighter', 1);
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[avion]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[avion] OFF;
GO

-- Table [dbo].[hangar] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[hangar]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[hangar] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[hangar] ([id_hangar], [capacidad], [codigo], [costo], [ubicacion]) VALUES
(1, 20, N'HG-001', 10000, N'Zona 4');
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[hangar]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[hangar] OFF;
GO

-- Table [dbo].[estadia] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[estadia]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[estadia] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[estadia] ([id_estadia], [avion], [costo], [fecha_fin], [fecha_in], [hangar]) VALUES
(2, 1, 304583.3333333333, N'2022-07-24T21:47:00.000', N'2022-06-24T10:47:00.000', 1);
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[estadia]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[estadia] OFF;
GO

-- Table [dbo].[datoPersona] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[datoPersona]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[datoPersona] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[datoPersona] ([id_persona], [cedula], [nombre_completo]) VALUES
(1, N'90992842', N'Juan De Los Santos'), 
(2, N'45454', N'Carlos Almonte');
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[datoPersona]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[datoPersona] OFF;
GO

-- Table [dbo].[tipo_piloto] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[tipo_piloto]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[tipo_piloto] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[tipo_piloto] ([id_tipoPiloto], [descripcion], [tipo]) VALUES
(1, N'Piloto de pasajeros ', N'Piloto Comercial'), 
(2, N'Copiloto de pasajeros', N'Copiloto comercial');
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[tipo_piloto]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[tipo_piloto] OFF;
GO

-- Table [dbo].[piloto] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[piloto]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[piloto] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[piloto] ([id_piloto], [dato_piloto], [fecha_rev], [horas_vuelo], [licencia], [tipo]) VALUES
(1, 1, N'2009-06-24T11:18:00.000', 3000, N'wqwqw', 1), 
(2, 2, N'2010-07-25T11:19:00.000', 1000, N'tyyt', 2);
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[piloto]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[piloto] OFF;
GO

-- Table [dbo].[tipo_vuelo] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[tipo_vuelo]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[tipo_vuelo] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[tipo_vuelo] ([id_tipoVuelo], [descripcion], [tipo]) VALUES
(1, N'Vuelos de pasajeros', N'Comercial'), 
(2, N'Vuelos de cargas', N'Carga');
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[tipo_vuelo]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[tipo_vuelo] OFF;
GO

-- Table [dbo].[vuelo] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[vuelo]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[vuelo] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[vuelo] ([id_vuelo], [copiloto_aginado], [fecha], [numero], [piloto], [tipo]) VALUES
(1, 2, N'2022-06-24T01:19:00.000', N'VU-00001', 1, 1);
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[vuelo]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[vuelo] OFF;
GO

-- Table [dbo].[turno_empleado] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[turno_empleado]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[turno_empleado] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[turno_empleado] ([id_turnoEmpleado], [descripcion], [turno]) VALUES
(1, N'Trabaja de tarde', N'Tarde'), 
(2, N'Trabaja de noche', N'Noche');
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[turno_empleado]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[turno_empleado] OFF;
GO

-- Table [dbo].[propietario] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[propietario]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[propietario] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[propietario] ([id_propietario], [dato_propietario], [rif]) VALUES
(1, 1, N'adsasds');
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[propietario]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[propietario] OFF;
GO

-- Table [dbo].[tipo_empleado] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[tipo_empleado]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[tipo_empleado] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[tipo_empleado] ([id_tipoEmpleado], [descripcion], [tipo]) VALUES
(1, N'Quien supervisa los hangares', N'Supervisor');
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[tipo_empleado]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[tipo_empleado] OFF;
GO

-- Table [dbo].[empleado] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[empleado]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[empleado] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[empleado] ([id_empleado], [dato_empleado], [hangar], [salario], [tipo], [turno]) VALUES
(1, 1, 1, 30000, 1, 1);
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[empleado]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[empleado] OFF;
GO

-- Table [dbo].[__EFMigrationsHistory] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[__EFMigrationsHistory]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[__EFMigrationsHistory] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES
(N'20220622161347_CreateIdentitySchema', N'6.0.6');
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[__EFMigrationsHistory]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[__EFMigrationsHistory] OFF;
GO

-- Table [dbo].[AspNetRoles] --

-- Table [dbo].[AspNetUsers] --
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[AspNetUsers]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[AspNetUsers] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[AspNetUsers] ([Id], [AccessFailedCount], [ConcurrencyStamp], [Email], [EmailConfirmed], [LockoutEnabled], [LockoutEnd], [NormalizedEmail], [NormalizedUserName], [PasswordHash], [PhoneNumber], [PhoneNumberConfirmed], [SecurityStamp], [TwoFactorEnabled], [UserName]) VALUES
(N'1ebd2b8f-4e77-494f-bf28-5bc13a0cd54d', 0, N'3d6db626-6bcf-468f-a8a8-35c07094ecaa', N'admin@gmail.com', 1, 1, NULL, N'ADMIN@GMAIL.COM', N'ADMIN@GMAIL.COM', N'AQAAAAEAACcQAAAAEIBkeZnFXpj5MYVri5zKQfC/o8v2JMeDJgvqaQh+Xt8TswXbHGeaGUrmIr24Jnx+SQ==', NULL, 0, N'XMX5H74HFYKMTC3JBDEQKSEMWIPELXEO', 0, N'admin@gmail.com');
COMMIT;
IF OBJECTPROPERTY(OBJECT_ID('[dbo].[AspNetUsers]'), 'TableHasIdentity') = 1 SET IDENTITY_INSERT [dbo].[AspNetUsers] OFF;
GO

-- Table [dbo].[AspNetRoleClaims] --

-- Table [dbo].[AspNetUserClaims] --

-- Table [dbo].[AspNetUserLogins] --

-- Table [dbo].[AspNetUserRoles] --

-- Table [dbo].[AspNetUserTokens] --

EXEC sp_MSforeachtable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'
GO
