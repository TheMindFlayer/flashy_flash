create database systa_fly;

use systa_fly;

create table propulsion(
	id_propulsion int PRIMARY key identity(1,1),
	propulsion varchar(100),
	descripcion text

);

create table modelo(
	id_modelo int PRIMARY key  identity(1,1),
	codigo varchar(200),
	descripcion text,
	propulsion int foreign key REFERENCES propulsion(id_propulsion),
	motores int,
	peso float
);

CREATE table tipo_avion(
	id_tipoAvion int PRIMARY key  identity(1,1),
	tipo varchar(100),
	descripcion text
);

create table avion(
	id_avion int PRIMARY key  identity(1,1),
	siglas varchar(100),
	capacidad int,
	tipo int FOREIGN KEY REFERENCES tipo_avion(id_tipoAvion),
	modelo int foreign key references modelo(id_modelo)
);

create table hangar(
	id_hangar int PRIMARY key identity(1,1),
	codigo varchar(200),
	ubicacion VARCHAR(50),
	costo int,
	capacidad int
);

CREATE table estadia(
	id_estadia int PRIMARY key identity(1,1),
	fecha_in datetime,
	fecha_fin datetime,
	costo float,
	avion int FOREIGN key REFERENCES avion(id_avion),
	hangar int foreign key REFERENCES hangar(id_hangar)
);

create table datoPersona(
	id_persona int PRIMARY key identity(1,1),
	cedula varchar(20),
	nombre_completo varchar(200)

);

create table tipo_piloto(
	id_tipoPiloto int PRIMARY key identity(1,1),
	tipo varchar(100),
	descripcion text
);

create table piloto(
	id_piloto int primary key identity(1,1),
	licencia varchar(50),
	horas_vuelo int,
	fecha_rev datetime,
	dato_piloto int FOREIGN key REFERENCES datoPersona(id_persona),
	tipo int FOREIGN key REFERENCES tipo_piloto(id_tipoPiloto)
);

CREATE table tipo_vuelo(
	id_tipoVuelo int PRIMARY key  identity(1,1),
	tipo varchar(100),
	descripcion text
);

create table vuelo(
	id_vuelo int primary key identity(1,1),
	numero varchar(100),
	fecha datetime,
	tipo int foreign key references tipo_vuelo(id_tipoVuelo),
	piloto int FOREIGN key REFERENCES piloto(id_piloto),
	copiloto_aginado int FOREIGN key REFERENCES piloto(id_piloto),
	avion int FOREIGN key REFERENCES avion(id_avion)
);


create table turno_empleado(
	id_turnoEmpleado int primary key identity(1,1),
	turno varchar(75),
	descripcion text
);


CREATE table propietario(
	id_propietario int primary key identity(1,1),
	rif varchar(30),
	dato_propietario int foreign key REFERENCES datoPersona(id_persona)
);

create table avion_propietario(
	id_avionPropietario int PRIMARY key identity(1,1),
	propietario_id int FOREIGN key REFERENCES propietario(id_propietario),
	avion_id int FOREIGN key REFERENCES avion(id_avion)
);


create table tipo_empleado(
	id_tipoEmpleado int primary key identity(1,1),
	tipo varchar(50),
	descripcion text
);

CREATE table empleado(
	id_empleado int primary key identity(1,1),
	salario int,
	turno int FOREIGN key references turno_empleado(id_turnoEmpleado),
	tipo int FOREIGN key REFERENCES tipo_empleado(id_tipoEmpleado),
	dato_empleado int foreign key REFERENCES datoPersona(id_persona),
	hangar int FOREIGN key REFERENCES hangar(id_hangar)
);